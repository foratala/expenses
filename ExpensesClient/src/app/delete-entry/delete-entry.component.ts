import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntryService } from '../entry.service';
import { EntryElement } from '../interfaces/EntryElement';

@Component({
  selector: 'app-delete-entry',
  templateUrl: './delete-entry.component.html',
  styleUrls: ['./delete-entry.component.css']
})
export class DeleteEntryComponent implements OnInit {

  id;
  entry = {
    Description: '',
    IsExpense: false,
    Value: 0
  };

  constructor(private route: ActivatedRoute,
              private service: EntryService,
              private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.service.getEntry(this.id).subscribe((data: EntryElement) => {
      console.log(data);
      this.entry = data;
    });
  }

  cancel() {
    this.router.navigate(['/']);
  }

  confirm() {
    this.service.deleteEntry(this.id).subscribe( data => {
      console.log(data);
    });
  }
}
