import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EntryElement } from './interfaces/EntryElement';

@Injectable({
  providedIn: 'root'
})
export class EntryService {

  baseUrl: string = 'http://localhost:50003/api/entries/';

  constructor(private http: HttpClient) { }

  getEntry(id: number) {
    return this.http.get(this.baseUrl + id);
  }

  getAll() {
    return this.http.get(this.baseUrl);
  }

  createEntry(entry: EntryElement) {
    return this.http.post(this.baseUrl, entry);
  }

  updateEntry(id: number, entry: EntryElement) {
    return this.http.put(`${this.baseUrl}${id}`, entry);
  }

  deleteEntry(id: number) {
    return this.http.delete(this.baseUrl + id);
  }
}
