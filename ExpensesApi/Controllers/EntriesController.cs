﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using ExpensesApi.Data;
using ExpensesApi.Models;

namespace ExpensesApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class EntriesController : ApiController
    {
        public IHttpActionResult GetEntries()
        {
            using (var context = new AppDbContext())
            {
                try
                {
                    var entries = context.Entries.ToList();
                    return Ok(entries);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
        }

        public IHttpActionResult GetEntry(int id)
        {
            using (var context = new AppDbContext())
            {
                try
                {
                    var entry = context.Entries.SingleOrDefault(x => x.Id == id);
                    if (entry == null) return NotFound();
                    return Ok(entry);
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
        }

        [HttpPost]
        public IHttpActionResult PostEntry([FromBody] Entry entry)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            try
            {
                using (var context = new AppDbContext())
                {
                    context.Entries.Add(entry);
                    context.SaveChanges();
                }
                return Ok("Entry was created!");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut]
        public IHttpActionResult PutEntry(int id, [FromBody] Entry entry)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            if (id != entry.Id) return BadRequest();

            try
            {
                using (var context = new AppDbContext())
                {
                    var dbEntry = context.Entries.SingleOrDefault(x => x.Id == id);
                    if (dbEntry == null) return NotFound();

                    dbEntry.Description = entry.Description;
                    dbEntry.IsExpense = entry.IsExpense;
                    dbEntry.Value = entry.Value;

                    context.SaveChanges();
                }
                return Ok("Entry was updated!");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteEntry(int id)
        {
            try
            {
                using (var context = new AppDbContext())
                {
                    var dbEntry = context.Entries.SingleOrDefault(x => x.Id == id);
                    if (dbEntry == null) return NotFound();

                    context.Entries.Remove(dbEntry);
                    context.SaveChanges();
                }
                return Ok("Entry was deleted!");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}