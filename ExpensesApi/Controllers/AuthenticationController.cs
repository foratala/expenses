﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using ExpensesApi.Data;
using ExpensesApi.Models;
using Microsoft.IdentityModel.Tokens;

namespace ExpensesApi.Controllers
{
    [EnableCors("*", "*", "*")]
    [RoutePrefix("auth")]
    public class AuthenticationController : ApiController
    {
        [Route("login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody] User user)
        {
            if (string.IsNullOrEmpty(user.UserName) || string.IsNullOrEmpty(user.Password))
                return BadRequest("Enter username and password");

            try
            {
                using (var context = new AppDbContext())
                {
                    var exists = context.Users.Any(m => m.UserName == user.UserName && m.Password == user.Password);
                    if (exists) return Ok(CreateToken(user));

                    return BadRequest("Wrong credentials");
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Route("register")]
        [HttpPost]
        public IHttpActionResult Register([FromBody] User user)
        {
            try
            {
                using (var context = new AppDbContext())
                {
                    var exists = context.Users.Any(m => m.UserName == user.UserName);
                    if (exists) return BadRequest("User already exists");

                    context.Users.Add(user);
                    context.SaveChanges();
                }
                return Ok(CreateToken(user));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        private JwtPackage CreateToken(User user)
        {
            var jwtHandler = new JwtSecurityTokenHandler();
            var claims = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Email, user.UserName), 
            });

            const string secretKey = "your secret key goes here";
            var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(secretKey));
            var signinCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            var token = jwtHandler.CreateJwtSecurityToken(subject: claims, signingCredentials: signinCredentials);

            var tokenStr = jwtHandler.WriteToken(token);

            return new JwtPackage {UserName = user.UserName, Token = tokenStr};
        }
    }

    public class JwtPackage
    {
        public string Token { get; set; }
        public string UserName { get; set; }
    }
}
